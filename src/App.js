import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import Header from './component/Header';
import Menu from './component/Menu';
import AppRouter from './AppRouter';
import Footer from './component/Footer';

class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <div className="container-fluid mt-4">
          <div className="row">
            <div className="col-sm-12 col-md-4 col-lg-3">
              <Menu />
            </div>
            <div className="col-sm-12 col-md-8 col-lg-9">
              <AppRouter />
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default App;
