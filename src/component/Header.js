import React from 'react';
import { Navbar } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import '../style/Header.css';
import '../style/fork-ribbon.css';
import logo from '../image/logo.png';

class Header extends React.Component {
    render() {
        return (
            <div className="header">
                <Navbar className="text-center">
                    <NavLink to="/">
                        <img src={ logo } className="img-fluid header-logo" alt="Logo"/>
                    </NavLink>
                </Navbar>
            </div>
        );
    }
}

export default Header;
