import React from 'react';
import axios from 'axios';
import { Table } from 'reactstrap';
import '../style/CountryDetail.css';

class CountryDetail extends React.Component {
    state = {
        country: []
    }

    componentDidMount (props) {
        let name = this.props.match.params.name;
        let url = ('https://restcountries.eu/rest/v2/name/'+name);
        axios
          .get(url)
          .then(({ data }) => {
            this.setState({
              country: data
            });
          })
          .catch((err) => { })
    }

    render() {
        const Country = this.state.country.map((el, index) => {
        return <div key={index}>
            <div className="flex-flag mt-3">
                <img src={el.flag} alt="Flag" className="img-fluid mb-4" />
            </div>
            
            <div className="mt-4">
                <Table responsive className="tbl-country-detail">
                    <tbody>
                        <tr>
                            <th>Name</th>
                            <td>{el.name}</td>
                        </tr>
                        <tr>
                            <th>Area</th>
                            <td>{el.area}</td>
                        </tr>
                        <tr>
                            <th>Borders</th>
                            <td>{el.borders+" "}</td>
                        </tr>
                        <tr>
                            <th>Calling Code</th>
                            <td>{el.callingCodes}</td>
                        </tr>
                        <tr>
                            <th>Cioc</th>
                            <td>{el.cioc}</td>
                        </tr>
                        <tr>
                            <th>City</th>
                            <td>{el.capital}</td>
                        </tr>
                        <tr>
                            <th>Demonym</th>
                            <td>{el.demonym}</td>
                        </tr>
                        <tr>
                            <th>NativeName</th>
                            <td>{el.nativeName}</td>
                        </tr>
                        <tr>
                            <th>Population</th>
                            <td>{el.population}</td>
                        </tr>
                        <tr>
                            <th>Region</th>
                            <td>{el.region}</td>
                        </tr>
                        <tr>
                            <th>Sub Region</th>
                            <td>{el.subregion}</td>
                        </tr>
                        <tr>
                            <th>Time Zone</th>
                            <td>{el.timezones}</td>
                        </tr>
                        <tr>
                            <th>Top Level Domain</th>
                            <td>{el.topLevelDomain}</td>
                        </tr>
                    </tbody>
                </Table>
            </div>
        </div>
      });
  
      return <div>{Country}</div>;
    }
}

export default CountryDetail;
