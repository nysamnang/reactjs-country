import React from 'react';
import '../style/Footer.css';

class Footer extends React.Component {
    render() {
        return (
            <div className="footer mt-5">
                <ul>
                    <li>&copy; 2017 Country</li>
                    <li><a href="https://gitlab.com/NYSamnang/reactjs-country">view source code</a></li>
                    <li>API LICENSE TO <a href="https://restcountries.eu/" target="_blank" rel="noopener noreferrer">REST COUNTRIES</a></li>
                    <li>power by <a href="https://www.heroku.com/" target="_blank" rel="noopener noreferrer">heroku</a></li>
                </ul>
            </div>
        );
    }
}

export default Footer;
