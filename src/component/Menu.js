import React from 'react';
import { ListGroup, ListGroupItem } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import '../style/Menu.css';

class Menu extends React.Component {
  render() {
    return (
      <ListGroup className="mt-3 menu">
        <ListGroupItem style={{ backgroundColor: '#17a2b8', color: '#EFF' }} className="text-center"><h6 className="m-0">REGION</h6></ListGroupItem>
        <ListGroupItem>
          <NavLink to="/" activeClassName="active" exact={true}>ALL COUNTRIES</NavLink>
        </ListGroupItem>
        <ListGroupItem>
          <NavLink to="/africa" activeClassName="active">AFRICA</NavLink>
        </ListGroupItem>
        <ListGroupItem>
          <NavLink to="/americas" activeClassName="active">AMERICAS</NavLink>  
        </ListGroupItem>
        <ListGroupItem>
          <NavLink to="/asia" activeClassName="active">ASIA</NavLink> 
        </ListGroupItem>
        <ListGroupItem>
          <NavLink to="/europe" activeClassName="active">EUROPE</NavLink>
        </ListGroupItem>
        <ListGroupItem>
          <NavLink to="/oceania" activeClassName="active">OCEANIA</NavLink>
        </ListGroupItem>
      </ListGroup>
    );
  }
}

export default Menu;
